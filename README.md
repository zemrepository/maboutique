Procédure d'installation de l'environnement de développement de MaBoutique
========================

Ce projet est composé :
* d'un front-end avec Twitter Bootstrap et Twig (layout php)
* d'un back-end avec Symfony2
* d'une base de donnée NoSQL MongoDB

1) Installation des dépendances
-------------------------------------
Pour installer, il faut utiliser Composer pour gérer les dépendances.

    composer install 

2) Checking your System Configuration
-------------------------------------
Pour vérifier que Symfony2 est correctement installé, executer en ligne de commande le script suivant : 

    php app/check.php

Ce script doit retourner un code status à 0.

3) Execution des tests
-------------------------------------
Pour executer les tests, executer les commandes suivantes : 

```
php app/console doctrine:mongodb:fixtures:load
phpunit -c app
```

