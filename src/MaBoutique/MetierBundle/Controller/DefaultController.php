<?php

namespace MaBoutique\MetierBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MaBoutiqueMetierBundle:Default:index.html.twig', array('name' => $name));
    }
}
