<?php

namespace MaBoutique\MetierBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ProduitRepository extends DocumentRepository
{
	public function findAllOrderedByName()
    {
        return $this->createQueryBuilder()
            ->sort('name', 'ASC')
            ->getQuery()
            ->execute();
    }
}