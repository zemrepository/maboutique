<?php 

namespace MaBoutique\MetierBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MaBoutique\MetierBundle\Document\Produit;

class LoadProduitData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $produit = new Produit();
        $produit->setNom('Produit de test');
        $produit->setPrix('6.5');

        $manager->persist($produit);
        $manager->flush();
    }
}