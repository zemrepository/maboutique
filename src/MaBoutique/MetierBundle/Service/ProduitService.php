<?php

namespace MaBoutique\MetierBundle\Service;

use MaBoutique\MetierBundle\Service\ProduitService;
use MaBoutique\MetierBundle\Repository\ProduitRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProduitService 
{
	private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function recupereTousLesProduits() {
    	$products = $this->container->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('MaBoutiqueMetierBundle:Produit')
            ->findAllOrderedByName();

		return $products;
    }
}
