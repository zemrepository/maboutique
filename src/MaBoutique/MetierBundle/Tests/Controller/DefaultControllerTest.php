<?php

namespace MaBoutique\MetierBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use MaBoutique\MetierBundle\Service\ProduitService;

class DefaultControllerTest extends WebTestCase
{
    private $container;

    public function setUp() 
    {
        $client = static::createClient();
        $this->container = $client->getKernel()->getContainer();        
    }
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Fabien');

        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }

    public function testRecupereTousLesProduits()
    {
        $service = new ProduitService($this->container);
        $produits = $service->recupereTousLesProduits();
        $this->assertGreaterThanOrEqual(1, count($produits));
    }
}
